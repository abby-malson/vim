"
" MAIN CUSTOMIZATION FILE
"

" my customizations
set background=dark
set mouse=a
"set backspace=2
set whichwrap+=<,>,h,l
set hlsearch

" change the mapleader from | to ,
let mapleader=","

let g:html_tag_case='lowercase'
ab //* //*********************************************
ab --- --------------------------------------------
ab === ============================================

" Enable loading filetype and indentation plugins
filetype plugin on
filetype indent on
au FileType php set omnifunc=phpcomplete#CompletePHP

" Turn syntax highlighting on
syntax on

"hide ^M characters
match Ignore /\r$/ 
"
" GLOBAL SETTINGS
"

" Write contents of the file, if it has been modified, on buffer exit
set autowrite

" Allow backspacing over everything
set backspace=indent,eol,start

" Insert mode completion options
set completeopt=menu,longest,preview

" Use UTF-8 as the default buffer encoding
set enc=utf-8

" Remember up to 100 'colon' commmands and search patterns
set history=100

" Enable incremental search
set incsearch

" Always show status line, even for one window
set laststatus=2

" Jump to matching bracket for 2/10th of a second (works with showmatch)
set matchtime=2

" Don't highlight results of a search
" set nohlsearch

" Enable CTRL-A/CTRL-X to work on octal and hex numbers, as well as characters
set nrformats=octal,hex,alpha

" Use Shift-F9 to toggle 'paste' mode
set pastetoggle=<S-F9>

" Show line, column number, and relative position within a file in the status line
set ruler

" Scroll when cursor gets within 3 characters of top/bottom edge
set scrolloff=3

" Round indent to multiple of 'shiftwidth' for > and < commands
set shiftround

" Use 4 spaces for (auto)indent
set shiftwidth=4

" Show (partial) commands (or size of selection in Visual mode) in the status line
set showcmd

" When a bracket is inserted, briefly jump to a matching one
set showmatch

" Don't request terminal version string (for xterm)
set t_RV=""

" Use 4 spaces for <Tab> and :retab
set tabstop=4

" Write swap file to disk after every 50 characters
set updatecount=50

" Remember things between sessions
"
" '20  - remember marks for 20 previous files
" \"50 - save 50 lines for each register
" :20  - remember 20 items in command-line history 
" %    - remember the buffer list (if vim started without a file arg)
" n    - set name of viminfo file
set viminfo='20,\"50,:20,%,n~/.viminfo

" Use menu to show command-line completion (in 'full' case)
set wildmenu

" Set command-line completion mode:
"   - on first <Tab>, when more than one match, list all matches and complete
"     the longest common  string
"   - on second <Tab>, complete the next full match and show menu
set wildmode=list:longest,full

" Go back to the position the cursor was on the last time this file was edited
au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")|execute("normal `\"")|endif

" Fix my <Backspace> key (in Mac OS X Terminal)
"set t_kb
"fixdel

" Avoid loading MatchParen plugin
let loaded_matchparen = 1

" syntax checker
let g:syntastic_php_checkers=['php', 'phpcs', 'phpmd']

" netRW: Open files in a split window
let g:netrw_browse_split = 1

" statusline
set statusline=%{fugitive#statusline()}

"
" MAPPINGS
"

" previous tab
map <C-P> :tabp<CR>
" next tab
map <C-N> :tabn<CR>

" save changes
map ,s :w<CR>
" exit vim without saving any changes
map ,q :q!<CR>
" exit vim saving changes
map ,w :x<CR>
" switch to upper/lower window quickly
map <C-J> <C-W>j
map <C-K> <C-W>k
" switch to upper/lower window quickly
map <C-L> <C-W>l
map <C-H> <C-W>h
" use CTRL-F for omni completion
imap <C-F> " map CTRL-L to piece-wise copying of the line above the current one
imap <C-L> @@@<ESC>hhkywjl?@@@<CR>P/@@@<CR>3s
" map ,f to display all lines with keyword under cursor and ask which one to
" jump to
nmap ,f [I:let nr = input("Which one: ")<Bar>exe "normal " . nr ."[\t"<CR>
" use Shift-<F6> to toggle line numbers
nmap <silent> <S-F6> :set number!<CR>
" page down with <Space>
nmap <Space> <PageDown>
" open filename under cursor in a new window (use current file's working
" directory) 
nmap gf :new %:p:h/<cfile><CR>
" map <Alt-p> and <Alt-P> to paste below/above and reformat
nnoremap <Esc>P  P'[v']=
nnoremap <Esc>p  p'[v']=
" visual shifting (does not exit Visual mode)
vnoremap < <gv
vnoremap > >gv 

" Generic highlight changes
highlight Comment cterm=none ctermfg=Gray
highlight IncSearch cterm=none ctermfg=Black ctermbg=DarkYellow
highlight Search cterm=none ctermfg=Black ctermbg=DarkYellow
highlight String cterm=none ctermfg=DarkGreen
highlight treeDir cterm=none ctermfg=Cyan
highlight treeUp cterm=none ctermfg=DarkYellow
highlight treeCWD cterm=none ctermfg=DarkYellow
highlight netrwDir cterm=none ctermfg=Cyan

" Set up cscope options
if has("cscope")
	set csprg=/usr/bin/cscope
	set csto=0
	set cst
	set nocsverb
	cs add cscope.out
	set csverb
	map <C-_> :cstag <C-R>=expand("<cword>")<CR><CR>
	map g<C-]> :cs find 3 <C-R>=expand("<cword>")<CR><CR>
	map g<C-\> :cs find 0 <C-R>=expand("<cword>")<CR><CR>
endif


"
" NERDTree configuration
"

" Increase window size to 35 columns
let NERDTreeWinSize=35

" Tlist uses right window
let Tlist_Use_Right_Window = 1

" map <Shift>-<F7> to toggle NERDTree window
nmap <silent> <S-F7> :NERDTreeToggle<CR>
" map <Shift>-<F8> to toggle NERDTree window
nmap <silent> <S-F8> :TlistToggle<CR>

" Pathogen - Plugin management
call pathogen#infect() 

" use pathogen to easily modify the runtime path to include all
" plugins under the ~/.vim/bundle directory
call pathogen#helptags()
call pathogen#runtime_append_all_bundles()

" rainbow parenthesis
"au VimEnter * RainbowParenthesesToggle
"au Syntax * RainbowParenthesesLoadRound
"au Syntax * RainbowParenthesesLoadSquare
"au Syntax * RainbowParenthesesLoadBraces
"au Syntax * RainbowParenthesesLoadChevrons
"let g:rainbow_active = 1
"let g:rainbow_operators = 1

" Quickly edit/reload the vimrc file
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>

if $COLORTERM == 'gnome-terminal'
	set t_Co=256
endif

if &t_Co >= 256
	colorscheme vividchalk
endif


highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

set statusline=\ "
set statusline+=%1*%-25.80f%*\ " file name minimum 25, maxiumum 80 (right justified)
set statusline+=%2*
set statusline+=%h "help file flag
set statusline+=%r "read only flag
set statusline+=%m "modified flag
set statusline+=%w "preview flag
set statusline+=%*\ "
set statusline+=%3*[
set statusline+=%{strlen(&ft)?&ft:'none'} " filetype
set statusline+=]%*\ "
set statusline+=%4*%{fugitive#statusline()}%*\ " Fugitive
" set statusline+=%5*%{Rvm#statusline()}%*\ " RVM
set statusline+=%6*%{SyntasticStatuslineFlag()}%* " Syntastic Syntax Checking
set statusline+=%= " right align
set statusline+=%8*%-14.(%l,%c%V%)\ %<%P%* " offset
 
" Colors
" =================
" Add this after the call to `colorscheme` in your vimrc file or require it from another file. Whatever floats your boat. Modify and mix as necessary.
 
hi User1 gui=NONE ctermfg=White ctermbg=DarkGray guifg=#a7dfff guibg=#333333 " File name
hi User2 gui=NONE ctermfg=LightRed ctermbg=DarkGray guifg=#ff9999 guibg=#333333 " File Flag
hi User3 gui=NONE ctermfg=White ctermbg=DarkGray guifg=#ffffff guibg=#333333 " File type
hi User4 gui=NONE ctermfg=Green ctermbg=DarkGray guifg=#90ff90 guibg=#333333 " Fugitive
" hi User5 gui=NONE ctermfg=LightYellow ctermbg=DarkGray guifg=#ffffa0 guibg=#333333 " RVM
hi User6 gui=NONE ctermfg=White ctermbg=DarkRed guifg=#ffffff guibg=#af0000 " Syntax Errors
hi User7 gui=NONE ctermfg=White ctermbg=Yellow guifg=#ffff00 guibg=#333333
hi User8 gui=NONE ctermfg=Magenta ctermbg=DarkGray guifg=#99a0f9 guibg=#333333 " Position

fun! s:ToggleMouse()
    if !exists("s:old_mouse")
        let s:old_mouse = "a"
    endif

    if &mouse == ""
        let &mouse = s:old_mouse
        echo "Mouse is for Vim (" . &mouse . ")"
    else
        let s:old_mouse = &mouse
        let &mouse=""
        echo "Mouse is for terminal"
    endif
endfunction
